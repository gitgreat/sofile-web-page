//视频
const data = [
  {
    name: "CCTV",
    
    download: "https://tv.cctv.com/",
    icon: "png",
    mark: "#bf0614",
    msg: "权威视频站",
  },
  {
    name: "YouTube",
    
    download: "https://www.youtube.com/",
    icon: "svg",
    msg: "国外知名视频网站",
    text: "科学",
  },
  {
    name: "IMDb",
    
    download: "https://www.imdb.com/",
    icon: "svg",
    msg: "最受欢迎和最权威的电影、电视和名人内容来源",
  },
  {
    name: "新片场",
    
    download: "https://www.xinpianchang.com/",
    icon: "svg",
    msg: "汇聚全球原创优质视频及创作人",
  },
];

const newArr = {
  type: "视频",
  path: "video",
  describe: "拓展视野",
  data: data,
};

export default newArr;
