//视频
const data = [
  {
    name: "腾讯视频",
    system: ["Win", "Mac", "Web"],
    download: "https://v.qq.com/download.html",
    icon: "svg",
  },
  {
    name: "爱奇艺",
    system: ["Win", "Mac", "Web"],
    download: "https://www.iqiyi.com/appstore.html",
    icon: "png",
  },
  {
    name: "抖音",
    system: ["Win", "Mac", "Web"],
    download: "https://www.douyin.com/",
    icon: "svg",
    msg: "短视频平台",
  },
  {
    name: "快手",
    system: ["Web"],
    download: "https://www.kuaishou.cn/",
    icon: "svg",
    msg: "短视频平台",
  },
  {
    name: "哔哩哔哩",
    system: ["Win", "Mac", "Web"],
    download: "https://app.bilibili.com/",
    icon: "svg",
    msg: "弹幕视频网站",
  },
  {
    name: "优酷",
    system: ["Win", "Mac", "Web"],
    download: "https://youku.com/product/index",
    icon: "svg",
  },
];

const newArr = { type: "视频", path: "video", data: data };

export default newArr;
