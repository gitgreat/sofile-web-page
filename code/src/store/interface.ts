//类型
//一级分类
export interface CategoryType {
  [key: string]: any; // 这里的 any 可以根据实际情况替换为你期望的值的类型
  title: string; //分类名
  name: string; //路由名
  path: string; //URL名
  data: SonType[]; //数据
}
//二级分类
export interface SonType {
  type: string; //分类名
  path: string; //路由名
  data: DataType[]; //数据
  describe?: string; //子分类描述
}

//软件详细数据
export interface DataType {
  name: string; //软件名
  nameuser?: string; //媒体用
  download: string; //按钮链接
  system?: ("Win" | "Mac" | "Linux" | "Web")[];
  icon: string; //图标后缀
  mark?: string; //图标背景色;
  text?: string; //按钮文本
  state?: "gnu" | "free"; //软件状态开源或者免费
  msg?: string; //二级标题
  describe?: string; //软件描述
  num?: number; //统计用浏览次数
}

//多媒体数据
export interface DataMedium {
  title: string; //标题
  author: string[]; //作者
  country: string; //国家
  language: string; //语言
  time: string; //时间
  type: string[]; //类型
  link: string; //链接
  introduce: string; //描述
  number?: string; //编号
}

//数据注入
export type ActiveTab = {
  activeTab: number;
  changeTab: (tab: number) => void;
};

//路径类型
export type PathType = {
  pagename?: string;
  screen: string;
  page?: string;
};
