//站点导航
import War from "@/page/see/war.vue"; //军事
import Beauty from "@/page/see/beauty.vue"; //美女
const pageData = [
  { title: "军事", path: "war", describe: "军事相关", component: War },
  { title: "靓女", path: "beauty", describe: "靓女相关", component: Beauty },
];

export default pageData;
