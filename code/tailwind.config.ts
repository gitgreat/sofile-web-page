/** @type {import('tailwindcss').Config} */
export default {
  content: ["./index.html", "./src/**/*.{vue,js,ts,jsx,tsx}"],
  plugins: [],
  darkMode: "class", //黑暗模式切换
  theme: {
    colors: {
      // Configure your color palette here
      transparent: "transparent",
      current: "currentColor",
      
      BgHeader: "#ffffff",
      darkBgHeader: "rgba(0, 0, 0, 0.88);",

      darkBgBody: "#101214", //背景颜色

      BgCardHeader: "#f5f5f5", //模块头部背景颜色
      darkBgCardHeader: "#21272b", //模块头部背景颜色

      BgCard: "#ffffff",
      darkBgCard: "#161A1D", //模块主内容背景颜色

      darkBgCardSeparate: "#293035", //模块分隔线颜色

      //按钮背景色
      BgButton: "#1e62eb",
      darkBgButton: "#1e62eb",

      //按钮中文本颜色
      ColorButton: "#ffffff",
      darkColorButton: "#ffffff",

      //图标背景色
      BgIcon: "#0cbf5b",
      darkBgIcon: "#1e625b33",

      //table列表分隔色
      BgTable: "#f8fafc",
      darkBgTable: "#1e62eb33",

      //移动端模块背景色
      BgMobileCard: "#f7f9fa",
      darkBgMobileCard: "#101214",

      //一级标题
      OneTitle: "#000000",
      darkOneTitle: "#ffffff",
      //二级标题
      TwoTitle: "#334155",
      darkTwoTitle: "#dee4ea",
      //三级标题
      ThreeTitle: "#94a3b8",
      darkThreeTitle: "#aaaeb3",
    },
  },
};
