# 目的

常用网址导航

# 发布

- 发布前对非 svg 图标进行二次压缩，进一步控制体积
- 请删除 index.html 文件中的百度用统计代码

# 使用

## 前置条件

此项目主要使用 vite 和 vue3 技术开发，所以需要 node 环境，并且您会常用的 npm 操作，例如安装包、打包和预览

您也可以直接下载打包好的 dist 文件夹，上传到网站根目录即可

## 快速上手

1. 克隆本项目到本地
2. 安装依赖 npm i
3. 运行 npm run dev 预览效果
4. 运行 npm run build 打包
5. 运行 npm run preview 预览打包后的效果
6. 将打包后，生成的 dist 文件夹中的内容上传到网站根目录即可

## 自定义

> 采用数据与样式分离的形式

- 数据存放在 /src/data 中，
- 样式存放在 /src/page 中，
- 路由存放在 /src/router 中
- 工具函数和类型存放在 /src/store 中
- 复用框架和模块存放在 /src/components 中，
- svg 类型图标存放在 /src/assets/brand_svg_logo 中
- 颜色文件，存放在 /src/store/tool.ts 和 /tailwind.config.js 中
- 通用配置文件存放在 /src/store/config.ts 中
- LOGO 存放在 /public/LOGO.svg 中
- 其他类型图标存放在 /public/brand_logo 中
- 媒体类型图标存放在 /public/user_logo 中

### 功能页面

- 首页 /src/page/function/home.vue
- 搜索页 /src/page/function/search.vue
- 历史页 /src/page/function/record.vue
- 404 页 /src/page/function/notFound.vue

### 数据和页面的网址

存放应用程序的，区分 Win、Mac、Linux，Web 版本的，称为数据，存放在 /src/data/software 中
存放网址的，不区分系统版本的，使用页面类型展示，网址数据存放在 /src/data/page 中

### 自定义首页

首页的样式文件在这里 /src/page/home.vue
首页的数据文件在这里

### 伪静态

#### 宝塔

```php
location / {
  try_files $uri $uri/ /index.html;
}

```

# 优化改进

## 待实现

- 优化 Meta 信息
- 不定期优化体积较大的图片
- 所有图片使用 svg 格式
- 检查所有软件分类是否正常，暂时无法分类的放入其他分类下
- 完善 ts 类型
- 搜索页中，使用上下方向键选中需要的值
- 添加低版本浏览器升级提示
- 完善数据类型
- 添加手机分类、电脑分类、
- 可添加喜爱的网站，放在首页,拖动排序
- body 下方有空隙
- 搜索耳朵的主人，没有结果
- 可添加推荐/更新红点
- 检查有无多余 css 样式
- 站点分类没有指示信息
- 短链接跳转功能
- 无法搜索柠檬

## 已放弃

- 可自行调整左侧菜单位置（不准备做登录功能，只能依赖本地实现会增加用户心智负担）

# 使用资源

- 样式模版：https://www.tailwind-kit.com/components
- 样式模版：https://daisyui.com/
- SVG 图标处理：https://juejin.cn/post/7091253193988014110
- 外观参考：https://thinredline.com.cn/jobs
- 模块组件：https://uiverse.io
- PWA 技术：https://juejin.cn/post/7294554207096750090
- 模糊搜索：https://github.com/shayneo/vue-fuse
- 暗夜模式：https://cbec.com.cn/

## 分类参考

- 36 氪：https://www.36kr.com/
- 澎湃：https://www.thepaper.cn/

## 应用参考：

- https://www.coolist.net/windows/
- https://juejin.cn/post/7245469976878334008
- https://juejin.cn/post/7305371899139899419
- https://juejin.cn/post/7217820487203618876
- https://www.qijishow.com/down/navigation.html
- 笔记软件：https://www.zhihu.com/question/549956892/answer/3124168096
- MAC：https://appstorrent.ru/

## 网址参考

- https://zhuanlan.zhihu.com/p/140747121

## 应用推荐

- 果核：https://www.zhihu.com/people/applek-51

# 应用商店

## MAC

- 马可波罗：https://www.macbl.com/
- https://zh.okaapps.com/
- https://xclient.info/
- https://www.seemac.cn/

## 其他

- 腾讯柠檬：https://lemon.qq.com/lab/
- 荔枝数码：https://lizhi.shop/
- 小米应用商店：https://app.mi.com/
- 联想：https://lestore.lenovo.com/
- QQ：https://pc.qq.com/category/c4.html
- 360：https://baoku.360.cn/
- 华为：https://appgallery.huawei.com/Featured
- 下载吧：https://www.xiazaiba.com/
- 小众软件：https://www.appinn.com/
- 西西软件园：https://www.cr173.com/
- 国外-4：https://alternativeto.net/
- 谷歌扩展商店：https://chromewebstore.google.com/
- 方块插件扩展：https://www.fkxz.cn/

# 设计

- 统一圆角、颜色、字体尺寸

图标：https://www.iconfont.cn/collections/detail?spm=a313x.collections_index.i1.d9df05512.33333a81zhJPwM&cid=19238
64px #bfbfbf

- 参考列表布局：https://sspai.com/mall/all

## 配色

### 正常模式

### 黑暗模式

- 主文本：text-gray-200
- 辅助文本：text-gray-400
